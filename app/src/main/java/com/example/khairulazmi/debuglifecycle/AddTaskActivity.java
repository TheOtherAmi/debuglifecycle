package com.example.khairulazmi.debuglifecycle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by khairul-azmi on 10/09/2016.
 */
public class AddTaskActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_add_task_frame_layout,new AddTaskFragment())
                .commit();
    }
}

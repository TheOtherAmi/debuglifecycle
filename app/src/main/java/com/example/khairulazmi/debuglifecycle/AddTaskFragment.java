package com.example.khairulazmi.debuglifecycle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by khairul-azmi on 10/09/2016.
 */
public class AddTaskFragment extends Fragment {

    private EditText mTaskEditText;
    private Button mAddButton, mCancelButton;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_task,container,false);

        mTaskEditText = (EditText)rootView.findViewById(R.id.fragment_add_task_et_task);
        mAddButton = (Button)rootView.findViewById(R.id.fragment_add_task_btn_add_task);
        mCancelButton = (Button)rootView.findViewById(R.id.fragment_add_task_btn_cancel);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //When user clicks on Add button, return the task string to MainActivity
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Get the task user entred in the EditText
                String task = mTaskEditText.getText().toString();

                // Create a result Intent to put data to pass back to previous Activity
                Intent resultIntent = new Intent();

                //Put data into Intent in key-value pairs
                resultIntent.putExtra("task",task);

                // Set the result of AddTaskActivity to Activity.RESULT_OK
                // and resultIntent so that the previous activity can get the data
                // that we're going passing back
                getActivity().setResult(Activity.RESULT_OK,resultIntent);

                // "Finish AddTaskActivity, which will navigate the user back to the previous activity
                getActivity().finish();
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //set result to Activity.RESUL_CANCELED to signify that user finished
                // the activity by "cancelling"
                getActivity().setResult(Activity.RESULT_CANCELED);

                // "Finish AddTaskACtivity, which will navigate the user back to the
                // previous activity
                getActivity().finish();
            }
        });
    }
}

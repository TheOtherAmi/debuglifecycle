package com.example.khairulazmi.debuglifecycle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by khairul-azmi on 10/09/2016.
 */
public class TodoItemsFragment extends Fragment {

    private static final String TAG = "TodoItemsFragment";
    private static final int ADD_TASK_REQUEST = 1;
    private TextView mTextView;
    private LinearLayout mLinearLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_todo_items, container, false);

        mTextView = (TextView)rootView.findViewById(R.id.textView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_todo_items, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_item:
                Log.d(TAG, "onOptionsItemSelected: Add Item");
                Intent intent = new Intent(getContext(), AddTaskActivity.class);
                startActivityForResult(intent,ADD_TASK_REQUEST);
        }
        return super.onOptionsItemSelected(item);


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_TASK_REQUEST && resultCode == Activity.RESULT_OK){
            String task = data.getStringExtra("task");
            addTask(task);
            Log.d(TAG, "onActivityResult: "+task);

        }
        else {
            Log.d(TAG, "onActivityResult: False");
        }


    }

    private void addTask(String task) {

        mTextView.append(task + "\n");




    }
}
